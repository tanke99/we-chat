var socket = io('http://localhost:3000')
var username
var avatar

$('#login_avatar li').on('click',function(){
    $(this)
        .addClass('now')
        .siblings()
        .removeClass('now')
})

$('#loginBtn').on('click',function(){
    username = $('#username').val().trim()
    if (!username){
        alert('请输入用户名')
    }
    avatar = $('#login_avatar li.now img').attr('src')
    // console.log(username,avatar);
    socket.emit('login',{
        username: username,
        avatar: avatar
    })
})


//监听登陆失败的请求
socket.on('loginError', data => {
    alert('登陆失败了')
})

//监听登陆成功的请求
socket.on('loginSuccess', data => {
    // 需要显示聊天窗口 淡入效果
    // 需要隐藏登陆窗口 淡出效果
    $('.login_box').fadeOut()
    $('.container').fadeIn()
    //设置个人信息 显示在界面上
    $('.avatar_url').attr('src', data.avatar)
    $('.user-list .username').text(data.username)

    username = data.username
    avatar = data.avatar

})

