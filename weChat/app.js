const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);

//记录所有已经登陆过的用户
const users = []

//使用express处理静态资源，把public目录设置为静态资源目录
app.use(express.static('public'))

app.get('/', (req, res) => {
  // res.sendFile(__dirname + '/index.html');
  res.redirect('./index.html')
});

server.listen(3000, () => {
  console.log('listening on *:http://localhost:3000');
});

io.on('connection', (socket) => {
  console.log('a user connected');

  socket.on('login', data => {
    //判断，如果在data在users中存在，说明该用户登陆过了，不允许登录
    //如果data在user中不存在，说明用户没有登陆，允许登录
    let user = users.find(item => item.username === data.username)
    if (user) {
      //表示用户存在
      socket.emit('loginError', { msg: '登陆失败' })
      // console.log('登陆失败')
    } else {
      //表示用户不存在,把用户存入user数组
      users.push(data)
      //告诉用户，登陆成功
      socket.emit('loginSuccess', data)
      // console.log('登陆成功',users)

      //告诉所有人，有新用户加入到了聊天室，广播消息
      //socket.emit 给当前用户发消息 io.emit 给所有用户发消息
      io.emit('addUser', data)


      //告诉所有用户，当前聊天室用户列表以及数量
      io.emit('userList', users)

      //把登陆成功的用户信息存储起来
      //socket.username ? avatar 内置对象？ 不太像
      socket.username = data.username
      socket.avatar = data.avatar
    }
  })


  //用户断开连接功能
  //监听用户断开连接
  socket.on('disconnect', () => {
    //把当前用户信息从user中删除
    let idx = users.findIndex(item => item.username === socket.username)
    //删除掉断开连接的人
    users.splice(idx, 1)
    // 1.告诉所有人，有人离开了聊天室
    io.emit('deleteUser', {
      username: socket.username,
      avatar: socket.avatar
    })
    // 2.告诉所有人，userList发生更新
    io.emit('userList', users)
  })

  //监听聊天的消息
  socket.on('sendMessage', data => {
    //广播给所有用户
    // console.log(data);
    io.emit('receiveMessage', data)
  })

  //接受图片的信息
  socket.on('sendImage', data => {
    //广播给所有用户
    // console.log('接受到图片数据',data.username);
    io.emit('receiveImage', data)
  })

});
